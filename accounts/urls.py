from django.urls import path
from accounts.views import create_user, user_login, user_logout


urlpatterns = [
    path("signup/", create_user, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
]
